package com.dbsyncTaskTower.webservice;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@Configuration
@RestController
public class DataRestController {

	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public Map<String, Object> insertIndex(@RequestBody String indexData) throws IOException, ParseException {

		String resourceName = "config.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		String host = "";
		String index = "";
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();

		headers.add("Content-Type", "application/json;charset=UTF-8");

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			props.load(resourceStream);
			host = props.getProperty("host");
			index = props.getProperty("index");
		}
		final String uri = "http://" + host + ":9200/" + index;

		// System.out.println(host + " " + index);
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonArray = new JSONArray();
		try {
			jsonArray = (JSONArray) jsonParser.parse(indexData);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> returnobject = new HashMap<>();
			returnobject.put("status", "failure");
			returnobject.put("message", "Post body should contain JSONArray");

			return returnobject;

		}
		for (int j = 0; j < jsonArray.size(); j++) {
			JSONObject individualObject = (JSONObject) jsonArray.get(j);
			String temp = individualObject.toString();
			// System.out.println(temp);
			HttpEntity<String> request = new HttpEntity<String>(temp, headers);
			String response = "";
			try {

				response = restTemplate.postForObject(uri, request, String.class);
			} catch (Exception e) {
				e.printStackTrace();
				Map<String, Object> returnobject = new HashMap<>();
				returnobject.put("status", "failure");
				returnobject.put("message", "Connection Lost.");

				return returnobject;

			}
			

		}
		//System.out.println("Inserted" + jsonArray.size() + " Records");
		Map<String, Object> returnobject = new HashMap<>();
		returnobject.put("status", "success");
		returnobject.put("message", "Inserted " + jsonArray.size() + " Index(s)");
		

		return returnobject;

	}

	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public Map<String, Object> deleteIndex(@RequestBody String deleteData)
			throws IOException, ParseException, InterruptedException {
		
		String resourceName = "config.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		String host = "";
		String index = "";
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();

		headers.add("Content-Type", "application/json;charset=UTF-8");

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			props.load(resourceStream);
			host = props.getProperty("host");
			index = props.getProperty("index");
		}
		final String uri = "http://" + host + ":9200/" + index;
		System.out.println("URL " +uri);
		
		JSONParser jsonParser = new JSONParser();
		JSONObject requestObject = new JSONObject();
		try {
			requestObject = (JSONObject) jsonParser.parse(deleteData);
		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> returnobject = new HashMap<>();
			returnobject.put("status", "failure");
			returnobject.put("message", "Delete body should contain JSONObject");

			return returnobject;

		}

		String flagString = (String) requestObject.get("flag").toString();

		int flag = Integer.parseInt(flagString);

		// Task_Detail_id delete
		if (flag == 0) {
			String idString = (String) requestObject.get("id").toString();
			long requestId = Long.parseLong(idString);
			
			int size =50;
			String getIndexUri = uri + "/_search?df=Task_Detail_Id&q=" + requestId + "&_source=false&size=" +size;

			String response = restTemplate.getForObject(getIndexUri, String.class);
			

			JSONObject responseDelObj = (JSONObject) jsonParser.parse(response);
			JSONObject hitsObj = (JSONObject) responseDelObj.get("hits");

			long total = (Long) hitsObj.get("total");

			if (total > 0) {	//
				
				if (total <= size) {
					
					JSONArray hitsArray = (JSONArray) hitsObj.get("hits");
					for (int i = 0; i < hitsArray.size(); i++) {	//
						JSONObject hitsInternalObject = (JSONObject) hitsArray.get(i);	//
						String _id = hitsInternalObject.get("_id").toString();
						String deleteUri = uri + "/" + _id;
						System.out.println("DEL URL"+deleteUri);
						restTemplate.delete(deleteUri);
					}
					//System.out.println("Deleted!!");
					Map<String, Object> returnobject = new HashMap<>();
					returnobject.put("status", "success");
					returnobject.put("message", "Deleted"+ total+ " Index(s)");
					return returnobject;

				}
				else {
					int count = 0;
					JSONArray hitsArray = (JSONArray) hitsObj.get("hits");
					while (total != 0) {
						for (int i = 0; i < hitsArray.size(); i++) {
							JSONObject hitsInternalObject = (JSONObject) hitsArray.get(i);
							String _id = hitsInternalObject.get("_id").toString();
							String deleteUri = uri + "/" + _id;

							try {
								restTemplate.delete(deleteUri);
							} catch (Exception e) {
								continue;
							}
							count++;

						}
						String responseTemp = restTemplate.getForObject(getIndexUri, String.class);

						JSONObject responseDelTempObj = (JSONObject) jsonParser.parse(responseTemp);
						JSONObject hitsTempObj = (JSONObject) responseDelTempObj.get("hits");

						total = (Long) hitsTempObj.get("total");
					//	System.out.println(total);

						hitsArray = (JSONArray) hitsTempObj.get("hits");
					//	System.out.println("Hits Array Size " + hitsArray.size());

					}
					Map<String, Object> returnobject = new HashMap<>();
					returnobject.put("status", "success");
					returnobject.put("message", "Deleted" + count + " Index(s)");

				//	System.out.println("Deleted" + count + " Records");
					return returnobject;
				}

			}
			else if (total == 0) {
					
					Map<String, Object> returnobject = new HashMap<>();
					returnobject.put("status", "failure");
					returnobject.put("message", "No Index found");
					return returnobject;
			}
			Map<String, Object> returnobject = new HashMap<>();
			returnobject.put("status", "failure");
			returnobject.put("message", "No Index found");
			return returnobject;

		}
		// Task_id delete
		else {
			
			String idString = (String) requestObject.get("id").toString();
			long requestId = Long.parseLong(idString);
			
			int size = 50;
			String getIndexUri = uri + "/_search?df=Task_Id&q=" + requestId + "&_source=false&size=" + size;

			String response = restTemplate.getForObject(getIndexUri, String.class);

			// System.out.println(response);

			JSONObject responseDelObj = (JSONObject) jsonParser.parse(response);
			JSONObject hitsObj = (JSONObject) responseDelObj.get("hits");

			long total = (Long) hitsObj.get("total");

			if (total > 0) {

				if (total <= size) {
					JSONArray hitsArray = (JSONArray) hitsObj.get("hits");
					for (int i = 0; i < hitsArray.size(); i++) {
						JSONObject hitsInternalObject = (JSONObject) hitsArray.get(i);
						String _id = hitsInternalObject.get("_id").toString();
						//System.out.println(_id);
						String deleteUri = uri + "/" + _id;

						restTemplate.delete(deleteUri);

					}
				//	System.out.println("Deleted!!" + total + "Records");
					Map<String, Object> returnobject = new HashMap<>();
					returnobject.put("status", "success");
					returnobject.put("message", "Deleted " + total + " Index(s)");
					return returnobject;

				} else {
					int count = 0;
					JSONArray hitsArray = (JSONArray) hitsObj.get("hits");
					while (total != 0) {
						for (int i = 0; i < hitsArray.size(); i++) {
							JSONObject hitsInternalObject = (JSONObject) hitsArray.get(i);
							String _id = hitsInternalObject.get("_id").toString();
							String deleteUri = uri + "/" + _id;

							try {
								restTemplate.delete(deleteUri);
							} catch (Exception e) {
								continue;
							}
							count++;

						}
						String responseTemp = restTemplate.getForObject(getIndexUri, String.class);

						JSONObject responseDelTempObj = (JSONObject) jsonParser.parse(responseTemp);
						JSONObject hitsTempObj = (JSONObject) responseDelTempObj.get("hits");

						total = (Long) hitsTempObj.get("total");
					//	System.out.println(total);

						hitsArray = (JSONArray) hitsTempObj.get("hits");
					//	System.out.println("Hits Array Size " + hitsArray.size());

					}
					Map<String, Object> returnobject = new HashMap<>();
					returnobject.put("status", "success");
					returnobject.put("message", "Deleted " + count + " Index(s)");

				//	System.out.println("Deleted" + count + " Records");
					return returnobject;
				}

			} else {
				Map<String, Object> returnobject = new HashMap<>();
				returnobject.put("status", "failure");
				returnobject.put("message", "No Records Found");
			//	System.out.println("No records Found!!");
				return returnobject;

			}

		}
		

	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Map<String, Object> updateIndex(@RequestBody String updateData) throws IOException, ParseException {
		
		String resourceName = "config.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties props = new Properties();
		String host = "";
		String index = "";
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();

		headers.add("Content-Type", "application/json;charset=UTF-8");

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			props.load(resourceStream);
			host = props.getProperty("host");
			index = props.getProperty("index");
		}
		final String uri = "http://" + host + ":9200/" + index;

		JSONParser jsonParser = new JSONParser();
		JSONObject requestObject = new JSONObject();
		try {
			requestObject = (JSONObject) jsonParser.parse(updateData);

		} catch (Exception e) {
			e.printStackTrace();
			Map<String, Object> returnobject = new HashMap<>();
			returnobject.put("status", "failure");
			returnobject.put("message", "Request body should contain JSONObject");

			return returnobject;

		}

		
		String taskIdString = requestObject.get("id").toString();
		
		JSONObject newObject = new JSONObject();
		JSONObject doc = new JSONObject();

		long taskId = Long.parseLong(taskIdString);
	//	System.out.println(taskId);
		int size = 50;
		String getIndexUri = uri + "/_search?df=Task_Id&q=" + taskId + "&_source=false&size=" + size;

		String response = restTemplate.getForObject(getIndexUri, String.class);

		

		JSONObject responseDelObj = (JSONObject) jsonParser.parse(response);
		JSONObject hitsObj = (JSONObject) responseDelObj.get("hits");

		long total = (Long) hitsObj.get("total");

		if (total > 0) {
			Set<String> keySet = requestObject.keySet();

			for (String s : keySet) {
			//	System.out.println("Keys" + s);
				if (!s.toString().equals("id")) {
					doc.put(s, requestObject.get(s));
				}

			}
			newObject.put("doc", doc);

			

			if (total <= size) {
				JSONArray hitsArray = (JSONArray) hitsObj.get("hits");
				for (int i = 0; i < hitsArray.size(); i++) {
					JSONObject hitsInternalObject = (JSONObject) hitsArray.get(i);
					String _id = hitsInternalObject.get("_id").toString();
				//	System.out.println(_id);
					String updateUri = uri + "/" + _id + "/_update";

					HttpEntity<String> request = new HttpEntity<String>(newObject.toString(), headers);
					String updateResponse = restTemplate.postForObject(updateUri, request, String.class);
					

				}
				
																																																																															Map<String, Object> returnobject = new HashMap<>();
				returnobject.put("status", "success");
				returnobject.put("message", "Updated " + total + " Index(s)");
				return returnobject;

			} else {
				int count = 0;
				JSONArray hitsArray = (JSONArray) hitsObj.get("hits");
				while (total != 0) {
					
					for (int i = 0; i < hitsArray.size(); i++) {
						JSONObject hitsInternalObject = (JSONObject) hitsArray.get(i);
						String _id = hitsInternalObject.get("_id").toString();
						String updateUri = uri + "/" + _id + "/_update";

						HttpEntity<String> request = new HttpEntity<String>(newObject.toString(), headers);

						String responseUpdate = restTemplate.postForObject(updateUri, request, String.class);
						

						count++;
						total--;
					}
					String getNextIndexUri = uri + "/_search?df=Task_Id&q=" + taskId + "&_source=false&size=" + size
							+ "&from=" + count;
					String responseTemp = restTemplate.getForObject(getNextIndexUri, String.class);
				
					JSONObject responseDelTempObj = (JSONObject) jsonParser.parse(responseTemp);
					JSONObject hitsTempObj = (JSONObject) responseDelTempObj.get("hits");
					
					hitsArray = (JSONArray) hitsTempObj.get("hits");
					

				}
				Map<String, Object> returnobject = new HashMap<>();
				returnobject.put("status", "success");
				returnobject.put("message", "Updated " + count + " Index(s)");

				
				return returnobject;
			}

		} else {
			Map<String, Object> returnobject = new HashMap<>();
			returnobject.put("status", "failure");
			returnobject.put("message", "No Records Found");
			
			return returnobject;

		}

	}
}
